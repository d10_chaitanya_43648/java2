package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Voter;


@WebServlet(value = "/authenticate", loadOnStartup = 1)
public class VoterLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VoterDaoImpl voterDao;
	private CandidateDaoImpl candidateDao;
	
	public void init(ServletConfig config) throws ServletException {
		try {
			voterDao = new VoterDaoImpl();
			candidateDao = new CandidateDaoImpl();
		} catch (Exception e) {
			throw new ServletException("err in init : " + getClass().getName(), e);
		}
	}

	public void destroy() {
		try {
			voterDao.cleanUp();
			candidateDao.cleanUp();
		} catch (SQLException e) {
			throw new RuntimeException("err in destroy : " + getClass().getName(), e);
		}
	}

	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			
			String email = request.getParameter("em");
			String pwd = request.getParameter("pass");
			System.out.println(email);
			System.out.println(pwd);
			Voter voter = voterDao.authenticateUser(email, pwd);
			System.out.println(voter.toString());
			// check for valid login
			if (voter == null) 
				pw.print("<h5> Invalid Login , Please retry <a href='login.html'>Retry</a></h5>");
			else {
				// 1 . get HttpSession from WC(web container)
				HttpSession session = request.getSession();
				System.out.println("from auth page : session " + session.isNew());
				System.out.println("session id " + session.getId());
				
				// 2 . save validated user details under session scope
				session.setAttribute("user_details", voter);
				
				//add dao instances (ref) to the session scope
				session.setAttribute("voter_dao", voterDao);
				session.setAttribute("candidate_dao", candidateDao);
				if(voter.getVoterRole().equalsIgnoreCase("admin")) {
					System.out.println("!!! Hello Admin !!!");
				}else {
					if(voter.isVoterStatus())
						response.sendRedirect("status");
					else
						response.sendRedirect("candidate_list");
				}
			}

		} catch (Exception e) {
			throw new ServletException("err in do-post :" + getClass().getName(), e);
		}
	}

}
