package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Voter;


@WebServlet("/status")
public class StatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			HttpSession session = request.getSession();
			Voter vt = (Voter) session.getAttribute("user_details");
			
			if(vt.isVoterStatus()) {
				pw.print("<h5>!!! You have already voted !!!</h5>");
			}else {
				String selectedcandidate = request.getParameter("cdId");
				CandidateDaoImpl dao = (CandidateDaoImpl) session.getAttribute("candidate_dao");
				String sts = dao.incrementVotes(Integer.parseInt(selectedcandidate));
				if(sts.equalsIgnoreCase("incremented")) {
					VoterDaoImpl dao1 = (VoterDaoImpl) session.getAttribute("voter_dao");
					String st = dao1.updateVotingStatus(vt.getVoterId());
					if(st.equalsIgnoreCase("update")) {
						pw.print("<h3>Hello, "+vt.getVoterName()+" </h3>");
						pw.print("<h5>You have voted Sucessfully message !!!</h5>");
					}
				}
			}
			session.invalidate();
			pw.print("<h5><a href='login.html'>Visit Again</a></h5>");
			
		} catch (Exception e) {
			throw new ServletException("err in do-get of " + getClass().getName(), e);
		}
	}

}
