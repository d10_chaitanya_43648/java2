package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import pojos.Candidate;
import pojos.Voter;


@WebServlet("/candidate_list")
public class CandidateListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h5> !!! Welcome !!! </h5>");
			HttpSession hs = request.getSession();
			Voter vt = (Voter) hs.getAttribute("user_details");
			if (vt != null) {
				pw.print("<h5> Candidate List :  </h5>");
				CandidateDaoImpl dao = (CandidateDaoImpl) hs.getAttribute("candidate_dao");
				List<Candidate> candidates = dao.listCandidates();
				
				pw.print("<form action='status'>");
				
				for (Candidate c : candidates)
					pw.print("<input type='radio' name='cdId' value=" + c.getCandidateId() + "> " + c + "<br>");
				
				pw.print("<input type='submit' value='Vote'>");
				pw.print("</form>");

			} else
				pw.print("<h5> Session Tracking Fails : NO Cookies!!!!!</h5>");
			
		} catch (Exception e) {
			throw new ServletException("in do-get of " + getClass().getName(), e);
		}
	}

}
