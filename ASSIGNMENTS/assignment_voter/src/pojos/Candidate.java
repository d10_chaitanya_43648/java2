package pojos;

public class Candidate {
	private  int candidateId;
	private String candidateName, candidateParty;
	private int candidateVote;
	
	public Candidate() {
	}

	public Candidate(int candidateId, String candidateName, String candidateParty, int candidateVote) {
		this.candidateId = candidateId;
		this.candidateName = candidateName;
		this.candidateParty = candidateParty;
		this.candidateVote = candidateVote;
	}

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public String getCandidateName() {
		return candidateName;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public String getCandidateParty() {
		return candidateParty;
	}

	public void setCandidateParty(String candidateParty) {
		this.candidateParty = candidateParty;
	}

	public int getCandidateVote() {
		return candidateVote;
	}

	public void setCandidateVote(int candidateVote) {
		this.candidateVote = candidateVote;
	}
	
	public String toString() {
		return String.format("%-10d : %-30s : %-15s", this.candidateId, this.candidateName, this.candidateParty);
	}
}
