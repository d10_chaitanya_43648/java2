package dao;

import static utils.DBUtils.getDBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Candidate;

public class CandidateDaoImpl implements ICandidateDao {
	private Connection cn;
	private PreparedStatement pst1, pst2;
	
	public CandidateDaoImpl() throws ClassNotFoundException, SQLException {
		cn = getDBConnection();
		pst1 = cn.prepareStatement("SELECT * FROM candidates");
		pst2 = cn.prepareStatement("UPDATE candidates SET votes=votes+1 WHERE id=?");
		System.out.println("Candidate dao created....");
	}
	
	@Override
	public List<Candidate> listCandidates() throws SQLException {
		List<Candidate> list = new ArrayList<>();
		try (ResultSet rs = pst1.executeQuery();) {
			while (rs.next()) {
				Candidate c = new Candidate(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
				list.add(c);
			}
		}
		return list;
	}

	@Override
	public String incrementVotes(int candidateId) throws SQLException {
		pst2.setInt(1, candidateId);	
		int st = pst2.executeUpdate();
		if(st == 1)
			return "incremented";
		return "decremented";
	}

	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (cn != null)
			cn.close();
		System.out.println("Candidate dao cleaned up!");
	}

}
