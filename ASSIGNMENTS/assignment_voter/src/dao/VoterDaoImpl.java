package dao;

import static utils.DBUtils.getDBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pojos.Voter;

public class VoterDaoImpl implements IVoterDao {
	private Connection cn;
	private PreparedStatement pst1, pst2;
	
	public VoterDaoImpl() throws ClassNotFoundException, SQLException {
		cn = getDBConnection();
		pst1 = cn.prepareStatement("SELECT * FROM voters WHERE email=? AND password=?");
		pst2 = cn.prepareStatement("UPDATE voters SET status = 1 WHERE id = ?");
		System.out.println("Voter dao created....");
	}
	
	@Override
	public Voter authenticateUser(String email, String password) throws SQLException {
		pst1.setString(1, email);
		pst1.setString(2, password);
		try(ResultSet rs = pst1.executeQuery();){
			while(rs.next()) {
				Voter v = new Voter(rs.getInt(1), rs.getString(2), email, password, rs.getBoolean(5), rs.getString(6));
				System.out.println(v);
				return v;
			}
		}
		return null;
	}

	@Override
	public String updateVotingStatus(int voterId) throws SQLException {
		pst2.setInt(1, voterId);	
		int st = pst2.executeUpdate();
		if(st == 1)
			return "update";
		return "not_update";
	}

	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (cn != null)
			cn.close();
		System.out.println("Voter dao cleaned up!");
	}

}
