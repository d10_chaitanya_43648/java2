package dao;

import java.sql.SQLException;

import pojos.Voter;

public interface IVoterDao {
	Voter authenticateUser(String email,String password) throws SQLException;
	
	String updateVotingStatus(int voterId) throws SQLException;
}
