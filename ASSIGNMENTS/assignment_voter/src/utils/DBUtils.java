package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public interface DBUtils {
	//add a static method to return FIXED DB connection
		static Connection getDBConnection() throws ClassNotFoundException,SQLException
		{
			//load JDBC driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/javadb?useSSL=false&allowPublicKeyRetrieval=true", "java", "java");
		}
	}