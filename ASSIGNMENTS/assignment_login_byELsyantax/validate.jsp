<%@page import="pojo.User"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<%!//JSP declaration block
	HashMap<String, User> users;

	public void jspInit() {
		//create empty map
		users = new HashMap<>();
		//populate it
		users.put("Rama", new User("Rama", "1234", 24));
		users.put("Raj", new User("Raj", "2234", 27));
		users.put("Riya", new User("Riya", "1734", 32));
		System.out.println(" in init " + users);
	}%>
<%
		//get req data from clnt
	String name = request.getParameter("name");
	String pwd = request.getParameter("pass");
	//HashMap's get 
	User details = users.get(name);
	if (details != null) {
		//name : valid , chk pwd
		if (details.getPassword().equals(pwd)) {
			//successful login
			session.setAttribute("user_details",details);
			//send redirect
			response.sendRedirect("details.jsp");
		} else {
	%>
	<h5 style="color: red;">
		Invalid Password , Please <a href="login.jsp">Retry</a>
	</h5>
	<%
		}
	} else {
	%>
	<h5 style="color: red;">
		Invalid Name , Do you want to <a href="register.jsp">Register</a> ?
	</h5>
	<%
		}
	%>
</body>
</html>