package com.app.controller;

import java.util.Arrays;
import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/test")
public class TestController {
public TestController() {
	System.out.println("in ctor of test controller");
}
@GetMapping("/hello3")//@request mapping in get mapping 
public String testModelMap(Model modelAtrribute) {
	System.out.println("in model map "+modelAtrribute);
	modelAtrribute.addAttribute("ts",new Date()).addAttribute("number_list", Arrays.asList(1,2,3,4));
	System.out.println("in model map "+modelAtrribute);
	return "/test/display";
}
}
