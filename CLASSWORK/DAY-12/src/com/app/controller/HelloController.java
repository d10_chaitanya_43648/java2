package com.app.controller;

import java.time.LocalDate;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {
public HelloController() {
	System.out.println("in ctor of"+getClass().getName());
}
@RequestMapping("/hello")  //MANDATORY
public String sayHello()
{
	System.out.println("in say hello");
	return "/welcome";//Logical view name ----> Actual view , by V.R(view resolver) --/WEB-INF/views/welcome.jsp
}
@RequestMapping("/hello2")
public ModelAndView testModelAndView() {
	System.out.println("in test model and view");
	return new ModelAndView("/welcome", "time_stamp", LocalDate.now());
}


}
