package dao;


import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DButils.Dbutils;
import pojo.Voters;

public class VotersDao implements Closeable{
	private PreparedStatement stmtSelect;
	private Connection connection;
	
	public VotersDao() throws Exception {
		this.connection= Dbutils.getConnection();
		this.stmtSelect=this.connection.prepareStatement("SELECT * FROM voters");
	}

	@Override
	public void close() throws IOException {
		try {
			this.connection.close();
			this.stmtSelect.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		}
	
	public List<Voters> getvoter() throws SQLException {
		List<Voters> list = new ArrayList<>();
		
		try(ResultSet rs = this.stmtSelect.executeQuery();){
			while(rs.next()) {
				if(rs.getString(6).equalsIgnoreCase("voter")) {
					Voters v = new Voters(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getString(6));
					list.add(v);
				}
			}
		}
		return list;
	}
		public List<Voters> getAdmin() throws SQLException {
			List<Voters> list = new ArrayList<>();
			
			try(ResultSet rs = this.stmtSelect.executeQuery();){
				while(rs.next()) {
					if(rs.getString(6).equalsIgnoreCase("admin")) {
						Voters v = new Voters(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getString(6));
						list.add(v);
					}
			}
			
		}
			return list;
	}
	
	
	
}
