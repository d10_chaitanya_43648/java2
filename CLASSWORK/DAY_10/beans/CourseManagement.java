package beans;

import dao.CourseDaoImpl;
import java.util.List;

public class CourseManagement {

	private CourseDaoImpl courseDao;                   
	public CourseManagement() {
		System.out.println("in course bean ctor");
		courseDao=new CourseDaoImpl();
	}
	//add a B.L method to get all course names from dao layer n ret it to JSP
	public List<String> getAllCourseNames()
	{
		System.out.println("in JB : get all c names ");
		return courseDao.getAllCourseNames();
	}
}
