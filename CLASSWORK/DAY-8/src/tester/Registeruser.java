package tester;

import static utils.HibernateUtils.getSf;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.Userdao;
import pojo.Role;
import pojo.User;

public class Registeruser {

	public static void main(String[] args) {
		try(SessionFactory sf = getSf();Scanner sc = new Scanner(System.in)){
			Userdao usedao = new Userdao();
			System.out.println("enter alll the details :");
			User user = new User(sc.next(), sc.next(), sc.next(), sc.next(), Role.valueOf(sc.next().toUpperCase()), sc.nextDouble(), LocalDate.parse(sc.next()));
			System.out.println("reg status"+usedao.registerUser(user));
		}catch (Exception e) {
			e.printStackTrace();
		}


	}

}
