package com.app.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.pojos.Role;
import com.app.pojos.User;
import com.app.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController {
	// dependency : Service layer i/f
	@Autowired
	private IUserService userService;

	public UserController() {
		System.out.println("in ctor of " + getClass().getName());
	}

	// add request handling method to show login form
	@GetMapping("/login")
	// H.M : key : /user/login + method=get
	// value : UserController.showLoginForm
	public String showLoginForm() {
		System.out.println("in show login form");
		return "/user/login";// Actual view name : /WEB-INF/views/user/login.jsp
	}

	// add req handling method to process login form
	@PostMapping("/login")
	// H.M : key : /user/login + method=post
	// value : UserController.processLoginForm
	// How to tell SC to inject request params in req handling method : D.I
	// Anno : @RequestParam
	// SC : String email=request.getParameter("email"); // MATCH req param name with
	// method arg name
	// String pass=request.getParamter("password");
	public String processLoginForm(@RequestParam String email,
@RequestParam(name = "password") String pass,Model modelMap,HttpSession hs) {
		System.out.println("in process login form " + email + " " + pass+" "+modelMap);
		try {
			// Controller invokes --> service ---> DAO ---> DB
			User user = userService.authenticateUser(email, pass);
			//store validated user details under session scope
			hs.setAttribute("user_details", user);
			//=> valid login, chk the role
			if(user.getRole().equals(Role.ADMIN))
				return "redirect:/admin/list";
			//vendor login
			return "redirect:/vendor/details";// 
		} catch (RuntimeException e) {
			System.out.println("err in processing " + e);
			//add err mesg as a model atribute
			modelMap.addAttribute("message","Invalid Login , Please retry!!!!!");
			// in case of invalid login , forward user to login page
			return "/user/login";// Actual view name : /WEB-INF/views/user/login.jsp
		}

		
	}
}
