package com.app.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.pojos.User;
import com.app.service.IVenderListService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired 
	private IVenderListService service;
	public AdminController() {
		System.out.println("in ctor of " + getClass().getName());
	}
	
	
	//add req handling method to forward clnt to admin list page (showing vendor list)
	@GetMapping("/list")
	public String showVendorList(Model modelmap,HttpSession hs)
	{
		System.out.println("in show vendor list");
		List<User> list = service.getAllVendor();
		hs.setAttribute("vendor_list", list);
		return "/admin/list";
				
	}
}
