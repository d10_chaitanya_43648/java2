package pojos;

import java.time.LocalDate;
import javax.persistence.*;

/*
 * Add these Data members
 userId (PK) ,name,email,password,role(enum),confirmPassword, regAmount: subscription amount;
	 LocalDate/Date regDate;
	 byte[] image;
 */
@Entity // mandatory cls level annotation : to tell hibernate frmwork , whatever follows
		// is an entity (
//has separate db table , has separate identity : PK) , n has standalone / separate life cycle
@Table(name = "user_details")
public class User {
	
	private Integer userId;// "int" data type also works only with added autoboxing n upcasting conversions
//int ---> Integer ---> Serializable
	private String name, email, password, confirmPassword;
	private Role userRole;
	private double regAmount;
	private LocalDate regDate;
	private byte[] image;

	public User() {
		System.out.println("in user ctor");
	}

	public User(String name, String email, String password, String confirmPassword, Role userRole, double regAmount,
			LocalDate regDate) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.confirmPassword = confirmPassword;
		this.userRole = userRole;
		this.regAmount = regAmount;
		this.regDate = regDate;
	}

	// all setters n getters
	@Id // mandatory at property level(getter) => PK constraint
	// @GeneratedValue//=> automatic id generation : strategy=GenerationType.AUTO
	@GeneratedValue(strategy = GenerationType.IDENTITY) // => automatic id generation : constraint : auto_increment
	@Column(name = "user_id")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(length = 20) // varchar (20)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		System.out.println("in set name");
		this.name = name;
	}

	@Column(length = 20, unique = true) // varchar(20) , unique constraint
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(length = 25, nullable = false) // NOT NULL constraint
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Transient // skips this property from persistence (i.e no col generated)
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@Enumerated(EnumType.STRING) // to tell hibernate to create col type : varchar n store enum constant name
	@Column(name="user_role",length = 20)
	public Role getUserRole() {
		return userRole;
	}

	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}
	@Column(name="reg_amount")
	public double getRegAmount() {
		return regAmount;
	}

	public void setRegAmount(double regAmount) {
		this.regAmount = regAmount;
	}
	@Column(name="reg_date")//def col type for LocalDate : date
	public LocalDate getRegDate() {
		return regDate;
	}

	public void setRegDate(LocalDate regDate) {
		this.regDate = regDate;
	}
	@Lob//mysql col type : longblob (bin large object) 
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", email=" + email + ", userRole=" + userRole
				+ ", regAmount=" + regAmount + ", regDate=" + regDate + "]";
	}

}
