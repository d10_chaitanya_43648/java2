package dao;

import pojos.Role;
import pojos.User;
import org.hibernate.*;
import static utils.HibernateUtils.getSf;

import java.time.LocalDate;
import java.util.List;

public class UserDaoImpl implements IUserDao {

	@Override
	public String registerUser(User user) {
		// user : TRANSIENT
		String message = "user reg failed....";
		// get session from SF
		Session session = getSf().openSession();// opens a brand new session from SF
		Session session2 = getSf().openSession();// opens a brand new session from SF
		System.out.println(session == session2);// false
		System.out.println("is open " + session.isOpen() + " is connected with db cn " + session.isConnected());// t t
		// begin transaction
		Transaction tx = session.beginTransaction(); // db cn is pooled out n wrapped in hib session n NEW L1 cache is
														// created : separate part of the heap : references of pojos :
														// EMPTY
		try {
			session.save(user);// Session API : save/persist/saveOrUpdate/merge --> transient pojo -->
								// persistent pojo
			// user : PERSISTENT => part of L1 cache , it will gain db identity(insert) upon
			// commit
			tx.commit();// Hibernate performs auto dirty checking --> insert query
			message = "user reged : ID " + user.getUserId();
			System.out.println(
					"after commit : is open " + session.isOpen() + " is connected with db cn " + session.isConnected());// t
																														// t

		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			// re throw the same exc to the caller
			throw e;
		} finally {
			if (session != null) // close hibernate session : L1 cache is destroyed n pooled out db cn rets to
									// the pool
				session.close();
		}
		System.out.println(
				"after finally  : is open " + session.isOpen() + " is connected with db cn " + session.isConnected());// f
																														// f

		// user : Not a part of L1 cache BUT has matching rec in DB : DETACHED
		return message;
	}

	@Override
	public String registerUserWithGetCurntSession(User user) {
		// user : transient
		String message = "user reg failed....";
		// get session from SF : getCurrentSession
		Session session = getSf().getCurrentSession();// new session
		Session session2 = getSf().getCurrentSession();// existing session obj reted to the caller
		System.out.println(session == session2);// true
		// begin tx
		Transaction tx = session.beginTransaction();
		System.out.println(" is open " + session.isOpen() + " is connected with db cn " + session.isConnected());// t t
		try {
			Integer id = (Integer) session.save(user);// Serializable ---> Integer
			// user : persistent
			tx.commit();// Hib performs auto dirty checking(insert) , session closed implicitly(L1 cache
						// is destroyed, pooled out db cn rets to the pool)
			message = "User registered with ID " + id;
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed implicitly(L1 cache
			// is destroyed, pooled out db cn rets to the pool)
			throw e;
		}
		// user : detached
		System.out.println(" is open " + session.isOpen() + " is connected with db cn " + session.isConnected());// f f

		return message;
	}

	@Override
	public User getUserDetailsById(int userId) {
		User user = null;// user : not even transient (NA)
		// get Session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// Session API : public <T> T get(Class<T> cls,Serializable id) throws
			// HibernateException
			user = session.get(User.class, userId);// in case of invalid user id => user =null ,
			// in case valid user id , user : PERSISTENT
			user = session.get(User.class, userId);
			user = session.get(User.class, userId);
			user = session.get(User.class, userId);
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return user;
	}

	@Override
	public List<User> getAllUsers() {
		List<User> userList = null;
		String jpql = "select u from User u";
		// get Session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// create Query object n exec the same
			userList = session.createQuery(jpql, User.class).getResultList();
			// select query is fired , extract RST --processing RST --def ctor of POJO --map
			// rst data --> pojo(setters) -->pojo refs are added to L1 cache--> refs are
			// also added to the list
			// --> rets this popualted list to the caller
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return userList;
	}

	@Override
	public List<User> getAllSelectedUsers(LocalDate startDate, LocalDate endDate, Role userRole) {
		List<User> users = null;
		String jpql = "select u from User u where u.regDate between :start and :end and u.userRole=:rl";
		// get Session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// create query --> set all 3 named IN params --> exec query
			users = session.createQuery(jpql, User.class).setParameter("start", startDate).setParameter("end", endDate)
					.setParameter("rl", userRole).getResultList();
			// users => list of PERSISTENT pojos
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return users;// users => list of DETACHED pojos
	}

	@Override
	public List<String> getAllSelectedUserNames(LocalDate startDate, LocalDate endDate, Role userRole) {
	  String jpql="select u.name from User u where u.regDate between :start and :end and u.userRole=:rl";
	  List<String> names=null;
	  // get Session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			names=session.createQuery(jpql, String.class).
					setParameter("start", startDate).setParameter("end", endDate).
					setParameter("rl", userRole).getResultList();
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return names;
	}

	@Override
	public User getUserSignUp(String email, String password) {
		User user = null;
		String jpql = "select u from User u where u.email=:em and u.password=:pass";
		Session session = getSf().getCurrentSession();
		
		Transaction tx = session.beginTransaction();
		try {
			user = session.createQuery(jpql, User.class).setParameter("em", email).setParameter("pass", password).getSingleResult();
			tx.commit();
		}catch(RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return user;
	}

	@Override
	public List<String> giveEmailIdUnderRole(Role role) {
		List<String> list = null;
		String jpql = "select u.email from User u where u.userRole=:rle";
		
		Session session = getSf().getCurrentSession();
		
		Transaction tx = session.beginTransaction();
		try {
			list = session.createQuery(jpql, String.class).setParameter("rle", role).getResultList();
			tx.commit();
			
		}catch(RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return list;
	}

	@Override
	public String changePassword(String email, String oldPwd, String newPwd) {
		String msg = "change password failed";
		
		String jpql = "select u from User u where u.email=:em and u.password=:pass";
		
		Session session = getSf().getCurrentSession();
		Transaction tx = session.beginTransaction();
		User u = null;
		try {
			u = session.createQuery(jpql, User.class).setParameter("em", email).setParameter("pass", oldPwd).getSingleResult();
			
			u.setPassword(newPwd);
			
			tx.commit();
			msg = "Changing pwd successful";
			
		}catch(RuntimeException e) {
			if(tx!=null)
				tx.rollback();
			throw e;
		}
		return msg;
	}

	@Override
	public String applyDiscount(double discount, LocalDate date) {
		String msg="bulk update failed";
		String jpql = "update User u set u.regAmount=u.regAmount-:disc where u.regdate < :dt and u.userRole=:rl";
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			int updateCount = session.createQuery(jpql).setParameter("disc", discount).setParameter("dt", date)
					.setParameter("rl", Role.CUSTOMER).executeUpdate();
			tx.commit();
			msg = "Bulk updation successful for " + updateCount + " customers";
			
		}catch(RuntimeException e) {
			if(tx!=null)
				tx.rollback();
			throw e;
		}
		return msg;
	}
	
	
	

}
