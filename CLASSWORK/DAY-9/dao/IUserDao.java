package dao;

import java.time.LocalDate;
import java.util.List;

import pojos.Role;
import pojos.User;

public interface IUserDao {
	String registerUser(User user);
	String registerUserWithGetCurntSession(User user);
	//add a method to fetch user details by PK
	User getUserDetailsById(int userId);
	//add a method to fetch details of all users
	List<User> getAllUsers();
	//get all users registered between strt date n end date & under a specific role
	List<User> getAllSelectedUsers(LocalDate startDate,LocalDate endDate,Role userRole);
	//get  all user names registered between strt date n end date & under a specific role
	List<String> getAllSelectedUserNames(LocalDate startDate,LocalDate endDate,Role userRole);
	
	//user signup and login
	User getUserSignUp(String email,String password);
	
	//give emails under role specific
	List<String> giveEmailIdUnderRole(Role role);
	
	//DAy9 
	//add method to change the user password 
	String changePassword(String email,String oldPwd,String newPwd);
	
	//string apply discount to it customers
	String applyDiscount(double discount,LocalDate date);
}
