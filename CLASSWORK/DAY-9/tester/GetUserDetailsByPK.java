package tester;

import static utils.HibernateUtils.getSf;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;

public class GetUserDetailsByPK {

	public static void main(String[] args) {
		// invoke static method of HibernateUtils to trigger class loading => creation
		// of
		// the singleton SF
		try (SessionFactory sf = getSf(); Scanner sc = new Scanner(System.in)) {
			// create DAO instance
			UserDaoImpl userDao = new UserDaoImpl();
			
			System.out.println("Enter user i/ps : nm email pwd cpwd role regamt regdate");
				// invoke dao's method for getting user details user registration
			System.out.println("User Details " +userDao.getUserDetailsById(sc.nextInt()));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
