package tester;

import static utils.HibernateUtils.getSf;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;

public class RegisterUser {

	public static void main(String[] args) {
		// invoke static method of HibernateUtils to trigger class loading => creation
		// of
		// the singleton SF
		try (SessionFactory sf = getSf(); Scanner sc = new Scanner(System.in)) {
			// create DAO instance
			UserDaoImpl userDao = new UserDaoImpl();
			/*
			 * String name, String email, String password, String confirmPassword, Role
			 * userRole, double regAmount, LocalDate regDate
			 */
			System.out.println("Enter user i/ps : nm email pwd cpwd role regamt regdate");
			User user = new User(sc.next(), sc.next(), sc.next(), sc.next(), 
					Role.valueOf(sc.next().toUpperCase()),
					sc.nextDouble(), LocalDate.parse(sc.next()));//user : TRANSIENT (Not yet persistent)
			//exists only in heap , neither in L1 cache nor in db
			// invoke dao's method for user registration
			System.out.println("reg status : " + userDao.registerUser(user));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
