package tester;

import static java.time.LocalDate.parse;
import static pojos.Role.valueOf;
import static utils.HibernateUtils.getSf;

import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;

public class GetSelectedUserNames {

	public static void main(String[] args) {
		// invoke static method of HibernateUtils to trigger class loading => creation
		// of
		// the singleton SF
		try (SessionFactory sf = getSf(); Scanner sc = new Scanner(System.in)) {
			// create dao instance
			UserDaoImpl dao = new UserDaoImpl();
			System.out.println("Enter strt date , end date n user role ");
			dao.getAllSelectedUserNames(parse(sc.next()), parse(sc.next()), valueOf(sc.next().toUpperCase()))
					.forEach(System.out::println);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
