package tester;
import static utils.HibernateUtils.getSf;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.User;

public class GetAllUserDetails {

	public static void main(String[] args) {
		//invoke static method of HibernateUtils to trigger class loading => creation of
		//the singleton SF
		try(SessionFactory sf=getSf())
		{
			//create dao instance
			UserDaoImpl dao=new UserDaoImpl();
		dao.getAllUsers().forEach(System.out::println);
//			for(User u : dao.getAllUsers())
//				System.out.println(u);
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
