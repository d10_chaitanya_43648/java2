package pojo;

import java.time.LocalDate;
import javax.persistence.*;

@Entity //mandatory
@Table(name="user_details")
public class User {
private Integer userId;
private String name,email,password,confirmPassword;
private Role userRole;
private double regAmount;
private LocalDate regDate;
private byte[] image;
public User() {
	System.out.println("in ctor of user");
}

public User(String name, String email, String password, String confirmPassword, Role userRole, double regAmount,
		LocalDate regDate) {
	
	this.name = name;
	this.email = email;
	this.password = password;
	this.confirmPassword = confirmPassword;
	this.userRole = userRole;
	this.regAmount = regAmount;
	this.regDate = regDate;
}

@Id //mandatory
//@GeneratedValue //automatic id generation 
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="user_id")
public Integer getUserId() {
	return userId;
}
public void setUserId(Integer userId) {
	this.userId = userId;
}
@Column(length=20)
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
@Column(length=20,unique=true)
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
@Column(length=20,nullable = false)
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
@Transient
public String getConfirmPassword() {
	return confirmPassword;
}

public void setConfirmPassword(String confirmPassword) {
	this.confirmPassword = confirmPassword;
}

@Enumerated(EnumType.STRING)
@Column(name="user_role",length=20)
public Role getUserRole() {
	return userRole;
}
public void setUserRole(Role userRole) {
	this.userRole = userRole;
}
@Column(name="reg_amount")
public double getRegAmount() {
	return regAmount;
}
public void setRegAmount(double regAmount) {
	this.regAmount = regAmount;
}
@Column(name="reg_date")
public LocalDate getRegDate() {
	return regDate;
}
public void setRegDate(LocalDate regDate) {
	this.regDate = regDate;
}
@Lob
public byte[] getImage() {
	return image;
}
public void setImage(byte[] image) {
	this.image = image;
}
@Override
public String toString() {
	return "User [userId=" + userId + ", name=" + name + ", email=" + email + ", userRole=" + userRole + ", regAmount="
			+ regAmount + ", regDate=" + regDate + "]";
}

}
