package dao;

import pojo.User;
import org.hibernate.*;
import static utils.HibernateUtils.getSf;

public class Userdao implements IUserDao {

	@Override
	public String registerUser(User user) {
		String message = "user registration failed";
		Session session = getSf().openSession();
		
		Transaction tx = session.beginTransaction();
		try {
			session.save(user);
			tx.commit();
			message="user registered";
		}catch(RuntimeException e) {
			if(tx!=null)
				tx.rollback();
			throw e;
				
		}finally {
			if(session != null)
				session.close();
		}
		return message;
	}

}
