package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IVenderList;
import com.app.pojos.User;

@Service
@Transactional
public class VenderListService implements IVenderListService {
	
	@Autowired
	private IVenderList list;
	
	@Override
	public List<User> getAllVendor() {
		
		return list.getVenderlist();
	}

}
