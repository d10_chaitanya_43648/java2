package com.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomePageController {
	public HomePageController() {
		System.out.println("in home controller");
	}
	@RequestMapping("/")
	public String ShowIndexPage() {
		
		System.out.println("in show index page ");
		return "/index";
		
	}
}
