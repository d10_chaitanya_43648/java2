package com.app.service;

import java.util.List;

import com.app.pojos.User;

public interface IVenderListService {
	List<User> getAllVendor();
}
