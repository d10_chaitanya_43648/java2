package com.app.dao;

import java.util.List;

import com.app.pojos.User;

public interface IVenderList {
List<User> getVenderlist();
}
