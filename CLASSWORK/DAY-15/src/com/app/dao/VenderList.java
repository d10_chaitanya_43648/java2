package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Role;
import com.app.pojos.User;

@Repository
public class VenderList implements IVenderList {
	
	@Autowired
	private SessionFactory sf;
	
	@Override
	public List<User> getVenderlist() {
		String jpql = "select u from User u where u.role=:rl";
		
		return sf.getCurrentSession().createQuery(jpql,User.class).setParameter("rl", Role.VENDOR).getResultList();
	}

}
